# IDE New File Issue - Test

Test an Issue with the Web IDE when creating a new file having on already selected

## Issue Description 
When you have selected a file within the Web IDE then create a new file, you will no longer be able to select that first file in the tabs at the top or in the side bar folder browser.

## Details 
Validated on Gitlab 13.6.1, and Gitlab.com as of 2021-01-07

### Steps to Reproduce 
1. Open the Web IDE
2. Select any file and open it
3. Create a new file using the "New File" button at the top of the folder browser
4. Observe the new file created and selected in the top tabs for open files 
5. Attempt to click on the file you had previously selected, BEFORE you created the new file
6. Observe it will not select that file
7. Attempt to select the file in the left hand folder browser
8. Observe it will not select that file

## Observations
I believe this is because the url path is not changed when a new file is created, and therefore the Vue Router *thinks* that you are still viewing the initial file and not the new file, which you are actually viewing.

### Stack Trace
```
vue-router.esm.js:2008 Uncaught (in promise) NavigationDuplicated: Avoided redundant navigation to current location: "/project/sololegends/ide-new-file-issue-test/tree/master/-/README.md/".
    at _t (https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.registry.repositories-pages.ide-pages.projects.registry.repositories.32912836.chunk.js:6:16187)
    at e.Lt.confirmTransition (https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.registry.repositories-pages.ide-pages.projects.registry.repositories.32912836.chunk.js:6:19111)
    at e.Lt.transitionTo (https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.registry.repositories-pages.ide-pages.projects.registry.repositories.32912836.chunk.js:6:18467)
    at e.push (https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.registry.repositories-pages.ide-pages.projects.registry.repositories.32912836.chunk.js:6:21484)
    at https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.registry.repositories-pages.ide-pages.projects.registry.repositories.32912836.chunk.js:6:26247
    at new Promise (<anonymous>)
    at rs.Ht.push (https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.registry.repositories-pages.ide-pages.projects.registry.repositories.32912836.chunk.js:6:26210)
    at rs.push (https://assets.gitlab-static.net/assets/webpack/pages.ide.542fd04a.chunk.js:16:227703)
    at a.clickFile (https://gitlab.com/assets/webpack/ide_runtime.a01ae98e.chunk.js:1:65904)
    at click (https://gitlab.com/assets/webpack/ide_runtime.a01ae98e.chunk.js:1:66952)
```

## Workaround
Select any other file wich is NOT the file you had select before creating the new file, the inability to select the first file with go away.
